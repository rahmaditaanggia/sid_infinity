<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PaketController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//member
Route::redirect('/ ', 'LandingPage');
Route::get('/LandingPage', [LandingController::class, 'landing'])->name('LandingPage');

Route::middleware('pelanggan', 'auth', 'NoCache')->group(function () {
    //User
    Route::get('/dashboard', [LandingController::class, 'dashboard'])->name('dashboard');
    Route::get('/profilepelanggan', [ProfileController::class, 'profile'])->name('profilepelanggan');
    Route::get('/edit/{id}', [ProfileController::class, 'edit'])->name('edit');
    Route::post('/update', [ProfileController::class, 'update'])->name('update');

    Route::get('/detailpenjualan/{id_paket}', [PaymentController::class, 'detailjual'])->name('detailpenjualan');
    Route::get('/detailpaket', [PaketController::class, 'detailpaket'])->name('detailpaket');
    Route::get('/search', [SearchController::class, 'search'])->name('search');
    Route::get('/invoice/{id_pesanan}', [UserController::class, 'invoice'])->name('invoice');
    Route::get('/berhasil', [UserController::class, 'berhasil'])->name('berhasil');
    Route::get('/gagal', [UserController::class, 'gagal'])->name('gagal');

});

Route::middleware('auth', 'NoCache', 'admin')->group(function () {
    //Admin
    Route::get('/home', [AdminController::class, 'index'])->name('admin.home');
    Route::get('/member', [AdminController::class, 'member'])->name('admin.member');
    Route::get('/paket', [AdminController::class, 'paket'])->name('admin.paket');
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    //Crud User
    Route::get('/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/user/store', [UserController::class, 'store'])->name('user.store');
    Route::get('/user/edit/{id_pelanggan}', [UserController::class, 'edit'])->name('user.edit');
    Route::put('/user/update/{id_pelanggan}', [UserController::class, 'update'])->name('user.update');
    Route::delete('/user/delete/{id_pelanggan}', [UserController::class, 'destroy'])->name('user.destroy');

    //Crud Paket
    Route::get('/paket/create', [PaketController::class, 'create'])->name('paket.create');
    Route::post('/paket/store', [PaketController::class, 'store'])->name('paket.store');
    Route::get('/paket/edit/{id_paket}', [PaketController::class, 'edit'])->name('paket.edit');
    Route::put('/paket/update/{id_paket}', [PaketController::class, 'update'])->name('paket.update');
    Route::delete('/paket/delete/{id_paket}', [PaketController::class, 'destroy'])->name('paket.destroy');
});

Route::get('/lihat', [MailController::class, 'expired'])->name('exp');

require __DIR__ . '/auth.php';
