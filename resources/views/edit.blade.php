<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- External CSS libraries -->
    <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/DetailPenjualan') }}/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet"
        href="{{ asset('assets/DetailPenjualan') }}/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Google fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/ProfilePelanggan') }}/style.css">
</head>

<body>
    <div class="page-content page-container" id="page-content">
        <div class="padding">
            <div class="row container d-flex justify-content-center">
                <div class="col-xl-6 col-md-12">
                    <div class="card user-card-full">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-12 bg-c-lite-green user-profile">
                                <div class="card-block text-center text-white">
                                    <div class="m-b-25">
                                        <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius"
                                            alt="User-Profile-Image">
                                    </div>
                                    @foreach ($users as $u)
                                    <form action="/update" method="post" autocomplete="off">
                                        @csrf
                                    <input type="hidden" name="id_pelanggan" value="{{ $u->id_pelanggan }}">
                                    <p class="m-b-10 f-w-600">Nama:</p>
                                    <input type="text" id="name" name="name" value="{{ $u->name }}" required>                                    <p></p>
                                    <i class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="card-block">
                                    <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                                    <div class="d-flex justify-content-between">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Email</p>
                                            <input type="text" id="email" name="email" value="{{ $u->email }}" required>                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600" style="margin-left: 20%;">No_telp</p>
                                            <input type="text" id="no_telp" name="no_telp" value="{{ $u->no_telp }}" style="margin-left: 20%;" required>                                        </div>
                                        </div><br>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Alamat</p>
                                            <input type="text" id="alamat" name="alamat" value="{{ $u->alamat }}" required>                                        </div>
                                        </div>
                                        <div class="row">
                                            <table>
                                                <tr>
                                                    <th><a href="/profilepelanggan"><button type="button" class="btn btn-danger" style="margin-left: 7%">Back</button></a>
                                                        <button type="submit" class="btn btn-primary">Submit</button></a></th>
                                                </tr>
                                            </table>
                                        </div>
                            </div>
                                    </div><br>
                                </form>
                                    @endforeach

                                    <div class="row">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
