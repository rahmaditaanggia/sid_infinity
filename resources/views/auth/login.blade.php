<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login Pelanggan</title>
    <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="{{ asset('assets/LogReg') }}/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ asset('assets/LogReg') }}/csslogin/main.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/util.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/fonts/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/fonts/icon-font.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/vendor/animate.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/vendor/hamburgers.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/vendor/animsition.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/vendor/select2.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LogReg') }}/csslogin/vendor/daterangepicker.css" rel="stylesheet">

    <link href="{{ asset('assets/zoo') }}/css/bootstrap/_media.css" rel="stylesheet">
    <link href="{{ asset('assets/zoo') }}/css/bootstrap/mixins/_border-radius.css" rel="stylesheet">
    <link href="{{ asset('assets/zoo') }}/css/bootstrap/mixins/_reset-text.css" rel="stylesheet">
    <link href="{{ asset('assets/zoo') }}/css/bootstrap/mixins/_screen-reader.css" rel="stylesheet">
    <link href="{{ asset('assets/zoo') }}/css/bootstrap/mixins/_text-hide.css" rel="stylesheet">
    <link href="{{ asset('assets/zoo') }}/css/bootstrap/mixins/_visibility.css" rel="stylesheet">
    <link href="{{ asset('assets/zoo') }}/css/bootstrap/utilities/_streched-link.css" rel="stylesheet">



    <meta name="robots" content="noindex, follow">
    <script nonce="364a2381-61c3-4bf9-bd2a-09ccae372931">
        (function(w, d) {
            ! function(dp, dq, dr, ds) {
                dp[dr] = dp[dr] || {};
                dp[dr].executed = [];
                dp.zaraz = {
                    deferred: [],
                    listeners: []
                };
                dp.zaraz.q = [];
                dp.zaraz._f = function(dt) {
                    return async function() {
                        var du = Array.prototype.slice.call(arguments);
                        dp.zaraz.q.push({
                            m: dt,
                            a: du
                        })
                    }
                };
                for (const dv of ["track", "set", "debug"]) dp.zaraz[dv] = dp.zaraz._f(dv);
                dp.zaraz.init = () => {
                    var dw = dq.getElementsByTagName(ds)[0],
                        dx = dq.createElement(ds),
                        dy = dq.getElementsByTagName("title")[0];
                    dy && (dp[dr].t = dq.getElementsByTagName("title")[0].text);
                    dp[dr].x = Math.random();
                    dp[dr].w = dp.screen.width;
                    dp[dr].h = dp.screen.height;
                    dp[dr].j = dp.innerHeight;
                    dp[dr].e = dp.innerWidth;
                    dp[dr].l = dp.location.href;
                    dp[dr].r = dq.referrer;
                    dp[dr].k = dp.screen.colorDepth;
                    dp[dr].n = dq.characterSet;
                    dp[dr].o = (new Date).getTimezoneOffset();
                    if (dp.dataLayer)
                        for (const dC of Object.entries(Object.entries(dataLayer).reduce(((dD, dE) => ({
                                ...dD[1],
                                ...dE[1]
                            })), {}))) zaraz.set(dC[0], dC[1], {
                            scope: "page"
                        });
                    dp[dr].q = [];
                    for (; dp.zaraz.q.length;) {
                        const dF = dp.zaraz.q.shift();
                        dp[dr].q.push(dF)
                    }
                    dx.defer = !0;
                    for (const dG of [localStorage, sessionStorage]) Object.keys(dG || {}).filter((dI => dI
                        .startsWith("_zaraz_"))).forEach((dH => {
                        try {
                            dp[dr]["z_" + dH.slice(7)] = JSON.parse(dG.getItem(dH))
                        } catch {
                            dp[dr]["z_" + dH.slice(7)] = dG.getItem(dH)
                        }
                    }));
                    dx.referrerPolicy = "origin";
                    dx.src = "/cdn-cgi/zaraz/s.js?z=" + btoa(encodeURIComponent(JSON.stringify(dp[dr])));
                    dw.parentNode.insertBefore(dx, dw)
                };
                ["complete", "interactive"].includes(dq.readyState) ? zaraz.init() : dp.addEventListener(
                    "DOMContentLoaded", zaraz.init)
            }(w, d, "zarazData", "script");
        })(window, document);
    </script>
</head>

<body>
    <div class="limiter">
        <div class="container-login100"
            style="background-image: url({{ asset('assets/LandingPage') }}/assets/img/white.jpeg);">
            <div class="wrap-login100 p-t-30 p-b-50">
                <span class="login100-form-title p-b-41" style="font-family: 'Roboto', sans-serif; font-weight: bold;">
                    Account Login
                </span>

                <form method="POST" action="{{ route('login') }}" class="login100-form validate-form p-b-30 p-t-5">
                    @csrf
                    <div class="wrap-input100 validate-input" data-validate="Enter username" for="email":value="__('Email')">
                        <input class="input100" id="email" class="block mt-1 w-full" type="email" name="email"
                            :value="old('email')" required autofocus autocomplete="username"
                            style="font-family: 'Roboto', sans-serif; font-weight: bold;">
                        <span class="focus-input100" data-placeholder="&#xe82a;" :messages="$errors - > get('email')" class="mt-2"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Enter password" for="password"
                        :value="__('Password')">
                        <input class="input100" id="pass" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password"
                        style="font-family: 'Roboto', sans-serif; font-weight: bold;" ><span id="mybutton" onclick="change()" class="">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" style="position: relative; left:90%; top:-40px;"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                    <path fill-rule="evenodd"
                                        d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                </svg>
                            </span>
                        <span class="focus-input100" data-placeholder="&#xe80f;" :messages="$errors - > get('password')"
                            class="mt-2"></span>
                    </div>
                    <center>
                        <div class="flex items-center justify-end mt-4">
                            @if (Route::has('password.request'))
                                <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                    href="{{ route('password.request') }}">
                                    {{ __('Forgot your password?') }}
                                </a>
                            @endif
                        </div>
                        <div class="container-login100-form-btn m-t-32">
                            <div class="w-70">
                                <p style="font-family: 'Roboto', sans-serif; font-weight: bold;">Belum punya akun? <a
                                        href="/register"
                                        style="font-family: 'Roboto', sans-serif; font-weight: bold; color: rgb(0, 77, 128);">Buat
                                        disini!</a></p>
                                </label>
                    </center>
                    <div class="container-login100-form-btn m-t-32">
                        <button class="login100-form-btn" style="font-family: 'Roboto', sans-serif; font-weight: bold;">
                            {{ __('Log in') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>

    <script src="vendor/animsition/js/animsition.min.js"></script>

    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="vendor/select2/select2.min.js"></script>

    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>

    <script src="vendor/countdowntime/countdowntime.js"></script>

    <script src="js/main.js"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
  <script>
    function change() {

        var x = document.getElementById('pass').type;

        if (x == 'password') {

            document.getElementById('pass').type = 'text';
            
            document.getElementById('mybutton').innerHTML = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-slash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                                            <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z"/>
                                                            <path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>
                                                            </svg>`;
        }
        else {

            document.getElementById('pass').type = 'password';

            document.getElementById('mybutton').innerHTML = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                                            </svg>`;
        }
    }
</script>
    
    <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v84a3a4012de94ce1a686ba8c167c359c1696973893317"
        integrity="sha512-euoFGowhlaLqXsPWQ48qSkBSCFs3DPRyiwVu3FjR96cMPx+Fr+gpWRhIafcHwqwCqWS42RZhIudOvEI+Ckf6MA=="
        data-cf-beacon='{"rayId":"83fff03b2da69f91","version":"2023.10.0","token":"cd0b4b3a733644fc843ef0b185f98241"}'
        crossorigin="anonymous"></script>
</body>

</html>
