<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Detail</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- External CSS libraries -->
    <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/DetailPenjualan') }}/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet"
        href="{{ asset('assets/DetailPenjualan') }}/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Favicon icon -->

    <!-- Google fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/DetailPenjualan') }}/css/style.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- @TODO: replace SET_YOUR_CLIENT_KEY_HERE with your client key -->
    <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="SET_YOUR_CLIENT_KEY_HERE"></script>
</head>

<body>

    <!-- Invoice 6 start -->
    <div class="invoice-6 invoice-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="invoice-inner clearfix">
                        <div class="invoice-info clearfix" id="invoice_wrapper">
                            <div class="invoice-headar">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="invoice-logo">
                                            <!-- logo started -->
                                            <div class="logo">
                                                <img src="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png"
                                                    style="width: 50%; height:30%;" alt="logo">
                                            </div>
                                            <!-- logo ended -->
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="invoice-contact-us">
                                            <h1>SID_Infinity</h1>
                                            <ul class="link">
                                                <li>
                                                    <i class="fa fa-map-marker"></i>Gedung BITC
                                                </li>
                                                <li>
                                                    <i class="fa fa-envelope"></i> <a
                                                        href="mailto:sales@hotelempire.com">ndry@sid.net.id</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-phone"></i> <a
                                                        href="tel:+55-417-634-7071">081296967484</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach ($paket as $p)
                                <div class="invoice-contant">
                                    <div class="invoice-top">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h1 class="invoice-name">DETAIL PAKET</h1>
                                            </div>
                                            <div class="col-sm-6 mb-30">
                                                <div class="invoice-number-inner">
                                                    <h2 class="name">HARGA :</h2>
                                                    <p class="mb-0">Rp. {{ number_format($p->harga) }}<span></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 mb-30">
                                                <div class="invoice-number">
                                                    <h4 class="inv-title-1">NAMA PAKET : </h4>
                                                    <h2 class="name mb-10">{{ $p->nama_paket }}</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-30">
                                            <div class="invoice-number">
                                                <h4 class="inv-title-1">KELEBIHAN PAKET : </h4>
                                                <p class="invo-addr-1 mb-0">
                                                    <i class="bi bi-check"></i> ------- <br />
                                                    <i class="bi bi-check"></i> ------- <br />
                                                    <i class="bi bi-check"></i> ------- <br />
                                                </p>
                                            </div>
                                        </div>
                                        <a id="pay-button" class="btn btn-dark" style="margin-left: 85%;">
                                            <i class="fa fa-checkout"></i> CHECKOUT
                                        </a>
                                    </div>
                                </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    <!-- Invoice 6 end -->

    {{-- <script type="text/javascript">
        // For example trigger on button clicked, or any time you need
        var payButton = document.getElementById('pay-button');
        payButton.addEventListener('click', function() {
            // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
            window.snap.pay('{{ $snapToken }}', {
                onSuccess: function(result) {
                    /* You may add your own implementation here */
                    alert("payment success!");
                    console.log(result);
                },
                onPending: function(result) {
                    /* You may add your own implementation here */
                    alert("wating your payment!");
                    console.log(result);
                },
                onError: function(result) {
                    /* You may add your own implementation here */
                    alert("payment failed!");
                    console.log(result);
                },
                onClose: function() {
                    /* You may add your own implementation here */
                    alert('you closed the popup without finishing the payment');
                }
            })
        });
    </script> --}}

    <script type="text/javascript">
        // For example trigger on button clicked, or any time you need
        var payButton = document.getElementById('pay-button');
        payButton.addEventListener('click', function () {
          // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token.
          // Also, use the embedId that you defined in the div above, here.
          window.snap.pay('{{ $snapToken }}', {
            onSuccess: function (result) {
            window.location.href = '/berhasil' 
              console.log(result);
            },
            onPending: function (result) {
              /* You may add your own implementation here */
              alert("Menunggu Pembayaran"); console.log(result);
            },
            onError: function (result) {
                window.location.href = '/gagal'
                console.log(result);
            },
            onClose: function () {
              alert('Anda menutup pop up tanpa membayar');
            }
          });
        });
      </script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jspdf.min.js"></script>
    <script src="assets/js/html2canvas.js"></script>
    <script src="assets/js/app.js"></script>
</body>

</html>
