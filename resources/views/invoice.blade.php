<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- DATE-PICKER -->
    <link rel="stylesheet" href="{{ asset('assets/invoice') }}/vendor/date-picker/css/datepicker.min.css">

    <!-- STYLE CSS -->
    <link rel="stylesheet" href="{{ asset('assets/invoice') }}/css/style.css">
    
    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700&display=swap" rel="stylesheet">
  
    
</head>

<body>
    
    <div class="wrapper">
        <form action="" id="wizard">
            <!-- SECTION 1 -->
            <h4></h4>
            <section>
                <h3>Detail Pembelian</h3>
                <div class="form-row">
                    <div class="form-col">
                        <label for="">
                            Full Name
                        </label>
                        <div class="form-holder">
                            <i class="zmdi zmdi-account-o"></i>
                            <input type="" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-col">
                        <label for="">
                            Tanggal
                        </label>
                        <div class="form-holder">
                            <i class="zmdi zmdi-edit"></i>
                            <input type="" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-col">
                        <label for="">
                            Email 
                        </label>
                        <div class="form-holder">
                            <i class="zmdi zmdi-email"></i>
                            <input type="" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-col">
                        <label for="">
                            Status
                        </label>
                        <div class="form-holder">
                            <i class="zmdi zmdi-calendar"></i>
                            <input type="" class="form-control datepicker-here" readonly>
                        </div>
                    </div>
                </div>
                <a href="/dashboard" class=""><button type="button"
                    class="btn btn-dark">Back</button></a>
            </section>

    </div>

    <script src="{{ asset('assets/invoice') }}/js/jquery-3.3.1.min.js"></script>

    <!-- JQUERY STEP -->
    <script src="{{ asset('assets/invoice') }}/js/jquery.steps.js"></script>

    <!-- DATE-PICKER -->
    <script src="{{ asset('assets/invoice') }}/vendor/date-picker/js/datepicker.js"></script>
    <script src="{{ asset('assets/invoice') }}/vendor/date-picker/js/datepicker.en.js"></script>

    <script src="{{ asset('assets/invoice') }}/js/main.js"></script>

    <!-- Template created and distributed by Colorlib -->
</body>

</html>
