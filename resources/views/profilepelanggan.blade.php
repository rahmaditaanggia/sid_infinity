<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- External CSS libraries -->
    <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/DetailPenjualan') }}/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet"
        href="{{ asset('assets/DetailPenjualan') }}/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Google fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/ProfilePelanggan') }}/style.css">
</head>

<body>
    <div class="page-content page-container" id="page-content">
        <div class="padding">
            <div class="row container d-flex justify-content-center">
                <div class="col-xl-6 col-md-12">
                    <div class="card user-card-full">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-12 bg-c-lite-green user-profile">
                                <div class="card-block text-center text-white">
                                    <div class="m-b-25">
                                        <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius"
                                            alt="User-Profile-Image">
                                    </div>
                                    <h6 class="f-w-600">{{ $nama_pelanggan }}</h6>
                                    <p></p>
                                    <i class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="card-block">
                                    <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                                    <div class="d-flex justify-content-between">
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Email</p>
                                            <h6 class="text-muted f-w-400">{{ $email_pelanggan }}</h6>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">No_telp</p>
                                            <h6 class="text-muted f-w-400">{{ $no_telp_pelanggan }}</h6>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="m-b-10 f-w-600">Alamat</p>
                                            <h6 class="text-muted f-w-400">{{ $alamat_pelanggan }}</h6>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <table>
                                            <tr>
                                                <th><a href="/dashboard"><button type="button" class="btn btn-primary" style="margin-left: 4%">Back</button></a>
                                                    <a href="/edit/{{ $id }}"><button type="button" class="btn btn-warning" style="margin-left: 2%">Edit</button></a></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
