<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Failed</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-qMX8Bo9eSTfRfJ1a3V6NsmgrzxBjMiLi0jgSWrejA+U19t0bq8kRyuymJUlaRGLkmh0AIs43L/sPux6FMw+ZcA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        .success-icon {
            position: relative;
            top: 10%;
            left: 50%;
            transform: translateX(-50%);
            background-color: #28a745;
            color: #fff;
            border-radius: 50%;
            width: 70px;
            /* Adjust the width to your preference */
            height: 70px;
            /* Adjust the height to your preference */
            line-height: 50px;
            /* Aligns the icon vertically */
            font-size: 2em;
        }
        .success-icon img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 50%;
        }
    </style>
</head>

<body style="background-color: #FFDAEC">
    <div class="d-flex align-items-center justify-content-center" style="height: 100vh;">
        <div class="card text-center" style="width: 30rem;">
            <br>
            <div class="success-icon"><img src="{{ asset('assets/LandingPage') }}/assets/img/gagal.png" alt=""></div>
            <div class="card-body">
                <h5 class="card-title">Pembayaran gagal</h5>
                <p class="card-text">Maaf, pembayaran Anda tidak berhasil. Silakan coba lagi </p>
                <p>{{ $tanggalPesan }}</p>
                <br>
                <a href="/dashboard" class="btn btn-danger">Go somewhere</a><br><br>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-eA0zPJSx2B+Ac6Vi/1ct4FFqkZ9tpJ2yAz9e3ta9/mjPXRxI/6bPj47E500Zgz0L" crossorigin="anonymous">
    </script>
</body>

</html>
        