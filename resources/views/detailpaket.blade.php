<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Detail Paket</title>
  <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/Dashboard') }}/style.css" rel="stylesheet">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Variables CSS Files. Uncomment your preferred color scheme -->
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-blue.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-green.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-orange.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-pink.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-purple.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-red.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}assets/css/main.css" rel="stylesheet">


<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1><span><img src="{{ asset('assets/LandingPage') }}/assets/img/Logo SID.png" style="width: 90%; margin-left:70%;"></span></a></h1>
      </div>
      <form action="{{ route('search') }}" method="GET" class="form-inline" style="margin-right:5%;">
        <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
      </form>
      
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link" href="#" style="width: 8%;">Sid Market</a></li>
        </ul>
        <div class="profile-icon">
          <img src="{{ asset('assets/LandingPage') }}/assets/img/akun.png" style="width: 48px; margin-top:2%;" alt="User Avatar" class="avatar">
          <div class="dropdown-content">
            <a href="/profilepelanggan">Profile</a>
            <a href="{{ route('logout') }}">Logout</a>
          </div>
        </div>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->


  <main id="main">
    <div class="row content">
      <div class="col-md-4" data-aos="fade-right">
          <li><i class=""></i></li>
        </ul>
        <p></p>
      </div>
    </div>
    <div class="row content">
        <div class="col-md-4" data-aos="fade-right">
            <li><i class=""></i></li>
          </ul>
          <p></p>
        </div>
      </div>

    <!-- ======= Pricing Section ======= -->
    <section id="features" class="features">
        <div class="container">
          
            <div class="section-title" data-aos="fade-up">
                <h2 style="font-color: black;">Features</h2>
                <p>Penawaran Terbatas</p>  
            </div>
  
            <div class="tab-pane active show" id="tab-1">
                <div class="row gy-4">
                  <div class="col-lg-12 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="100" >
                    <h3 style="background-color: rgba(193, 214, 239, 0.555);">beberapa paket terbaik</h3>
                    <div class="row" data-aos="fade-left">
                        @foreach ($paket as $p)
            
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                              <div class="box featured" data-aos="zoom-in" data-aos-delay="200">
                                  <h3>{{ $p->nama_paket }}</h3>
                                  <h4><sup>Rp</sup>{{ number_format($p->harga) }}<span> / month</span></h4>
                                  <ul>
                                      <li>Kecepatan internet hingga {{ $p->kecepatan }}</li>
                                      <li>Kuota keluarga 15 GB</li>
                                      <li>Unlimitited voice and sms</li>
                                      <li>Kuota orbit 20GB</li>
                                  </ul>
                                  <div class="btn-wrap">
                                      <a href="/detailpenjualan/{{ $p->id_paket }}" class="btn-buy">Buy Now</a>
                                  </div>
                              </div><br><br>
                            </div>
                            @endforeach
                          </div>
                        </div><br><br>
                          </div>
                          
                  <div class="col-lg-4 order-1 order-lg-2 text-center" data-aos="fade-up" data-aos-delay="200">
                  </div>
                
              </div><!-- End Tab Content 1 -->
              {{-- <div class="tab-pane" id="tab-2">
                <div class="row gy-4">
                  <div class="col-lg-12 order-2 order-lg-1">
                    <h3 style="background-color: rgba(146, 223, 195, 0.555);">Ini pasti Untung</h3>
                    <div class="row" data-aos="fade-left">
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box featuredd" data-aos="zoom-in" data-aos-delay="200">
                            <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>420.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan/" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
    
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box" data-aos="zoom-in" data-aos-delay="200">
                          <span class="advanced">Advanced</span>
                          <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>390.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
    
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box" data-aos="zoom-in" data-aos-delay="200">
                          <span class="advanced">Advanced</span>
                            <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>430.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
    
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box featuredd" data-aos="zoom-in" data-aos-delay="200">
                            <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>460.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
    
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box featuredd" data-aos="zoom-in" data-aos-delay="200">
                            <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>490.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
    
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box featuredd" data-aos="zoom-in" data-aos-delay="200">
                            <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>500.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
    
                      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                        <div class="box featuredd" data-aos="zoom-in" data-aos-delay="200">
                            <h3>Paling Untung</h3>
                            <h4><sup>Rp</sup>440.000<span> / month</span></h4>
                            <ul>
                                <li>Kecepatan internet hingga 50 Mbps</li>
                                <li>Kuota keluarga 15 GB</li>
                                <li>Unlimitited voice and sms</li>
                                <li>Kuota orbit 20GB</li>
                            </ul>
                            <div class="btn-wrap">
                                <a href="/detailpenjualan" class="btn-buyy">Buy Now</a>
                            </div>
                        </div><br><br>
                      </div>
                  </div>
                  </div>
                  <div class="col-lg-4 order-1 order-lg-2 text-center">
                  </div>
                </div>
              </div><!-- End Tab Content 2 -->
    
              <div class="tab-pane" id="tab-3">
                <div class="row gy-4">
                  <div class="col-lg-12 order-2 order-lg-1">
                    <h3 style="background-color: rgba(158, 185, 128, 0.938);">Paling Populer</h3>
                    <div class="row" data-aos="fade-left">
    
                        <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                          <div class="box" data-aos="zoom-in" data-aos-delay="200">
                            <span class="advanced">Advanced</span>
                            <h3>Populer</h3>
                                <h4><sup>Rp</sup>600.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featureddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Populer</h3>
                                <h4><sup>Rp</sup>630.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featureddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Populer</h3>
                                <h4><sup>Rp</sup>650.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featureddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Populer</h3>
                                <h4><sup>Rp</sup>680.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box" data-aos="zoom-in" data-aos-delay="200">
                              <span class="advanced">Advanced</span>
                                <h3>Populer</h3>
                                <h4><sup>Rp</sup>700.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featureddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Populer</h3>
                                <h4><sup>Rp</sup>740.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featureddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Populer</h3>
                                <h4><sup>Rp</sup>780.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featureddd" data-aos="zoom-in" data-aos-delay="200">
                              <h3>Populer</h3>
                                <h4><sup>Rp</sup>800.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
                        </div>
                  </div>
                  <div class="col-lg-4 order-1 order-lg-2 text-center">
                  </div>
                </div>
              </div><!-- End Tab Content 3 -->
    
              <div class="tab-pane" id="tab-4">
                <div class="row gy-4">
                  <div class="col-lg-12 order-2 order-lg-1">
                    <h3 style="background-color: rgba(168, 134, 190, 0.347);">Serba Complete</h3>
                    <div class="row" data-aos="fade-left">
    
                        <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                          <div class="box" data-aos="zoom-in" data-aos-delay="200">
                            <span class="advanced">Advanced</span>
                            <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>800.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>840.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>860.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>880.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>900.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>930.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>950.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>980.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box featuredddd" data-aos="zoom-in" data-aos-delay="200">
                                <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>970.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
    
                          <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                            <div class="box" data-aos="zoom-in" data-aos-delay="200">
                              <span class="advanced">Advanced</span>
                              <h3>Pasti Complete</h3>
                                <h4><sup>Rp</sup>1.000.000<span> / month</span></h4>
                                <ul>
                                    <li>Kecepatan internet hingga 50 Mbps</li>
                                    <li>Kuota keluarga 15 GB</li>
                                    <li>Unlimitited voice and sms</li>
                                    <li>Kuota orbit 20GB</li>
                                </ul>
                                <div class="btn-wrap">
                                    <a href="/detailpenjualan" class="btn-buyyyy">Buy Now</a>
                                </div>
                            </div><br><br>
                          </div>
                        </div>
                  </div> --}}
                  <div class="col-lg-4 order-1 order-lg-2 text-center">
                  </div>
                </div>
              </div><!-- End Tab Content 4 -->
            </div>
          </div>
        </div>
      </section><!-- End Pricing Section -->
  


        

    <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>SID Infinity</h3>
              <p class="pb-3"><em>Memperluas jaringan internet di daerah anda</em></p>
              <p><br>
                <strong>Phone:</strong> 081296967484<br>
                <strong>Email:</strong> ndry@sid.net.id<br>
              </p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div>

        </div>
      </div>
    </div>

    
  </footer><!-- End Footer -->
  <script>
    window.onclick = function(event) {
  if (!event.target.matches('.profile-icon')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    for (var i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.style.display === 'block') {
        openDropdown.style.display = 'none';
      }
    }
  }
}
  </script>


  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/aos/aos.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/Dashboard') }}/assets/js/main.js"></script>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.cjs.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.esm.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/LandingPage') }}/assets/js/main.js"></script>


</body>

</html>
