<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Landing Page</title>
    <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="{{ asset('assets/LandingPage') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/LandingPage') }}/assets/css/style.css" rel="stylesheet">


<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center header-transparent">
        <div class="container d-flex align-items-center justify-content-between">

            <div class="logo">
                <h1><span><img src="{{ asset('assets/LandingPage') }}/assets/img/Logo SID.png"
                            style="width: 130%;"></span></a></h1>
            </div>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#about">About</a></li>
                    <li><a class="nav-link scrollto" href="#features">Payment</a></li>
                    <li><a class="nav-link scrollto" href="#maps">Maps</a></li>
                    <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->
            <a href="/login"><button class="tombol">LOGIN / REGISTER</button></a>
        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">

        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-7 pt-5 pt-lg-0 order-2 order-lg-1 d-flex align-items-center">
                    <div data-aos="zoom-out">
                        <img src="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png"
                            style="height: 80px; width:50%;">
                        <h1>Build Your Internet Network With <span>SID INFINITY</span></h1>
                        <h2>We are a maker of fast and reliable internet network services</h2>
                        <div class="text-center text-lg-start">
                            <a href="#features" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 order-1 order-lg-2 hero-img" data-aos="zoom-out" data-aos-delay="300">
                    <img src="{{ asset('assets/LandingPage') }}/assets/img/view.png" class="img-fluid animated"
                        style="height: 100%; width:100%;">
                </div>
            </div>
        </div>

        <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
            viewBox="0 24 150 28 " preserveAspectRatio="none">
            <defs>
                <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
            </defs>
            <g class="wave1">
                <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
            </g>
            <g class="wave2">
                <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
            </g>
            <g class="wave3">
                <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
            </g>
        </svg>

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= Details Section ======= -->
        <section id="about" class="about">
            <div class="container">

                <div class="row content">
                    <div class="col-md-4" data-aos="fade-right">
                        <img src="{{ asset('assets/LandingPage') }}/assets/img/details-1.png" class="img-fluid"
                            alt="">
                    </div>
                    <div class="col-md-8 pt-4" data-aos="fade-up">
                        <h3>Solusi Jaringan Terpercaya Untuk Bisnis Anda</h3>
                        <p class="fst-italic">
                            Kami menawarkan solusi jaringan terbaik untuk memenuhi kebutuhan komunikasi dan konektivitas
                            bisnis Anda. Dengan layanan kami, Anda dapat mengoptimalkan kinerja jaringan Anda,
                            meningkatkan keamanan data, dan memastikan ketersediaan sistem yang handal.
                        </p>
                        <ul>
                            <li><i class="bi bi-check"></i> Kinerja Optimal: Jaringan kami dirancang untuk memberikan
                                kinerja maksimal, sehingga bisnis Anda dapat beroperasi dengan lancar tanpa hambatan
                                jaringan.</li>
                            <li><i class="bi bi-check"></i> Keamanan Terjamin: Kami menyediakan solusi keamanan jaringan
                                canggih untuk melindungi data sensitif Anda dari ancaman cyber.</li>
                            <li><i class="bi bi-check"></i> Skalabilitas: Layanan kami dapat disesuaikan dengan
                                kebutuhan bisnis Anda, baik itu untuk bisnis kecil, menengah, maupun besar.</li>
                            <li><i class="bi bi-check"></i> Dukungan 24/7: Tim dukungan kami siap membantu Anda kapan
                                pun diperlukan, sehingga Anda dapat menjalankan bisnis tanpa khawatir.</li>
                        </ul>
                        <p>
                            Hubungi kami sekarang untuk mendapatkan konsultasi gratis dan mulailah meningkatkan kinerja
                            jaringan bisnis Anda. </p>
                    </div>
                </div>
        </section>

                   <!-- ======= Pricing Section ======= -->
    <section id="features" class="features">
        <div class="container">
  
          <div class="section-title" data-aos="fade-up">
            <h2>Pricing</h2>
            <p>Check our Pricing</p>
          </div>
  
          <div class="row" data-aos="fade-left">
  
            <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
              <div class="box featured" data-aos="zoom-in" data-aos-delay="200">
                <h3>Penawaran Terbaik</h3>
                <h4><sup>Rp</sup>420.000<span> / month</span></h4>
                <ul>
                  <li>Kecepatan internet hingga 50 Mbps</li>
                  <li>Kuota keluarga 15 GB</li>
                  <li>Unlimitited voice and sms</li>
                  <li>Kuota orbit 20GB</li>
                </ul>
                <div class="btn-wrap">
                  <a href="/login" class="btn-buy">Buy Now</a>
                </div>
              </div>
            </div>
  
            <div class="col-lg-3 col-md-6 mt-4 mt-md-0" >
              <div class="box featured" data-aos="zoom-in" data-aos-delay="200">
                <h3>Paling Untung</h3>
                <h4><sup>Rp</sup>500.000<span> / month</span></h4>
                <ul>
                  <li>Paket Jitu 11P 100 Mbps</li>
                  <li>Cocok digunakan untuk maks 10 perangkat</li>
                  <li>bebas biaya pasang</li>
                  <li></li>
                </ul>
                <div class="btn-wrap">
                  <a href="/login" class="btn-buy">Buy Now</a>
                </div>
              </div>
            </div>
  
            <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
              <div class="box" data-aos="zoom-in" data-aos-delay="400">
                <span class="advanced">Advanced</span>
                <h3>Paling Populer</h3>
                <h4><sup>Rp</sup>550.000<span>/ month</span></h4>
                <ul>
                  <li>Paket JITU 1 2P-TV 50 Mbps</li>
                  <li>Cocok digunakan untuk maks 5 perangkat</li>
                  <li>Paket TV 81 Channel</li>
                  <li></li>
                </ul>
                <div class="btn-wrap">
                  <a href="/login" class="btn-buy">Buy Now</a>
                </div>
              </div>
            </div>
  
            <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
              <div class="box" data-aos="zoom-in" data-aos-delay="400">
                <span class="advanced">Advanced</span>
                <h3>Paket Complete</h3>
                <h4><sup>Rp</sup>600.000<span>/ month</span></h4>
                <ul>
                  <li>Kecepatan internet hingga 100 Mbps</li>
                  <li>Kuota keluarga 15GB untuk 6 nomor</li>
                  <li>N30 menit voice & 30 SMS seluruh operator</li>
                  <li></li>
                </ul>
                <div class="btn-wrap">
                  <a href="/login" class="btn-buy">Buy Now</a>
                </div>
              </div>
            </div>
  
          </div>
  
        </div>
      </section><!-- End Pricing Section -->

                <!-- ======= Testimonials Section ======= -->
                <section id="maps" class="maps">
                    <div class="container" data-aos="fade-up">

                        <div class="section-title">
                            <h2>Lokasi</h2>
                            <p>MY LOCATION</p>
                        </div>
                    </div>

                    <div data-aos="fade-up">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d990.2406800549755!2d107.53167656609425!3d-6.895062693822146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e56fca03dc83%3A0xc6c42797bedcc686!2sPT.%20ForIT%20Asta%20Solusindo%20-%20SIDNet!5e0!3m2!1sid!2sid!4v1704268989476!5m2!1sid!2sid"
                            width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
            </div>
            </div>
        </section><!-- End Testimonials Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>Contact</h2>
                    <p>Contact Us</p>
                </div>

                <div class="row">

                    <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
                        <div class="info">
                            <div class="address">
                                <i class="bi bi-geo-alt"></i>
                                <h4>Location:</h4>
                                <p>Gedung BITC (Baros Information Technology And Creativity Center ) Jl. HMS Mintareja
                                    Sarjana Hukum, Baros, Kec. Cimahi Tengah</p>
                        
                                <i class="bi bi-envelope" style="position: relative; left:130%; top:-95px;"></i>
                                <h4 style="position: relative; left:130%; top:-95px;">Email:</h4>
                                <p style="position: relative; left:130%; top:-93px;">ndry@sid.net.id</p>
                         
                                <i class="bi bi-phone" style="position: relative; left:210%; top:-150px;"></i>
                                <h4 style="position: relative; left:210%; top:-150px;">Call:</h4>
                                <p style="position: relative; left:210%; top:-147px;">081296967484</p>
                            </div>

                        </div>

                    </div>

                    
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

     <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>SID Infinity</h3>
              <p class="pb-3"><em>Memperluas jaringan internet di daerah anda</em></p>
              <p><br>
                <strong>Phone:</strong> 081296967484<br>
                <strong>Email:</strong> ndry@sid.net.id<br>
              </p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div>

        </div>
      </div>
    </div>

    
  </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/LandingPage') }}/assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.js"></script>
    <script src="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets/LandingPage') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="{{ asset('assets/LandingPage') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="{{ asset('assets/LandingPage') }}/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/LandingPage') }}/assets/js/main.js"></script>

</body>

</html>
