<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Dashboard</title>
  <link href="{{ asset('assets/LandingPage') }}/assets/img/infiniti.png" rel="icon">
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{ asset('assets/LandingPage') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/Dashboard') }}/style.css" rel="stylesheet">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Variables CSS Files. Uncomment your preferred color scheme -->
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-blue.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-green.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-orange.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-pink.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-purple.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}/assets/css/variables-red.css" rel="stylesheet">
  <link href="{{ asset('assets/Dashboard') }}assets/css/main.css" rel="stylesheet">


<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1><span><img src="{{ asset('assets/LandingPage') }}/assets/img/Logo SID.png" style="width: 90%; margin-left:70%;"></span></a></h1>
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link" href="#" style="width: 8%;">Sid Market</a></li>
        </ul>
        <div class="profile-icon">
          <img src="{{ asset('assets/LandingPage') }}/assets/img/akun.png" style="width: 48px; margin-top:2%;" alt="User Avatar" class="avatar">
          <div class="dropdown-content">
            <a href="/profilepelanggan">Profile</a>
            <a href="{{ route('logout') }}">Logout</a>
          </div>
        </div>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->


  <main id="main">
    <div class="row content">
      <div class="col-md-4" data-aos="fade-right">
          <li><i class=""></i></li>
        </ul>
        <p></p>
      </div>
    </div>



        <!-- ======= F.A.Q Section ======= -->
        <section id="faq" class="faq section-bg">
          <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2></h2>
          <p>Ayo Beli Paket Jaringan Internet <a href="{{ route('exp') }}">Anda</a> !</p>
        </div>
      </div>

    <section id="testimonials" class="testimonials" style="background-image: url({{ asset('assets/LandingPage') }}/assets/img/ungu.jpeg);">
      <div class="container" >

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                <h1>PAKET TERBAIK</h1>
                <h3>hanya di sid infinity</h3>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                <h1>PALING UNTUNG</h1>
                <h3>Dijamin untung ga akan rugi</h3>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                <h1>SANGAT POPULER</h1>
                <h3>Cepat Beli keburu kehabisan</h3>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                <h1>COMPLETE BANGET</h1>
                <h1>dijamin puas internetan seharian</h1>
              </div>
            </div><!-- End testimonial item -->
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Features Section ======= -->
 <section id="features" class="features">
  <div class="container" data-aos="fade-up">

    <ul class="nav nav-tabs row gy-4 d-flex">

      <li class="nav-item col-6 col-md-4 col-lg-3" >
        <a class="nav-link active show" data-bs-toggle="tab" data-bs-target="#tab-1" style="background-color: rgb(188, 190, 211);">
          <i class="bi bi-binoculars color-cyan"></i>
          <h4>Sisa Waktu</h4>
        </a>
      </li><!-- End Tab 1 Nav -->

      <li class="nav-item col-6 col-md-4 col-lg-3">
        <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-2" style="background-color: rgb(174, 223, 199);">
          <i class="bi bi-box-seam color-indigo"></i>
          <h4>Untung</h4>
        </a>
      </li><!-- End Tab 2 Nav -->

      <li class="nav-item col-6 col-md-4 col-lg-3">
        <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-3" style="background-color: rgb(205, 222, 158);">
          <i class="bi bi-brightness-high color-teal"></i>
          <h4>Populer</h4>
        </a>
      </li><!-- End Tab 3 Nav -->

      <li class="nav-item col-6 col-md-4 col-lg-3">
        <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-4" style="background-color: rgb(230, 192, 192);">
          <i class="bi bi-command color-red"></i>
          <h4>Complete</h4>
        </a>
      </li><!-- End Tab 4 Nav -->

    </ul>
  </section>

    <!-- ======= Pricing Section ======= -->
    <section id="features" class="features">
      <div class="container">

          <div class="section-title" data-aos="fade-up">
              <p>Check our Pricing</p>
              <a href="/detailpaket" class=""><button type="button" class="btn btn-warning" style="margin-left: 80%; width:18%;"><b>Lihat semua paket </b> <i class="bi bi-arrow-right"></i></button></a>

          </div>

          <div class="row" data-aos="fade-left">
            @foreach ($paket as $p)

              <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                  <div class="box featured" data-aos="zoom-in" data-aos-delay="200">
                      <h3>{{ $p->nama_paket }}</h3>
                      <h4><sup>Rp</sup>{{ number_format($p->harga) }}<span> / month</span></h4>
                      <ul>
                          <li>Kecepatan internet hingga {{ $p->kecepatan }}</li>
                          <li>Kuota keluarga 15 GB</li>
                          <li>Unlimitited voice and sms</li>
                          <li>Kuota orbit 20GB</li>
                      </ul>
                      <div class="btn-wrap">
                          <a href="/detailpenjualan/{{ $p->id_paket }}" class="btn-buy">Buy Now</a>
                      </div>
                  </div><br><br>
                </div>
                @endforeach
              </div>
            </div><br><br>
          </div>
        </div>
      </div>
    </section><!-- End Pricing Section -->



        <div class="tab-content">


          <div class="tab-pane" id="tab-5">
            <div class="row gy-4">
              <div class="col-lg-8 order-2 order-lg-1">
                <h3>Adipiscing</h3>
                <p>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                  culpa qui officia deserunt mollit anim id est laborum
                </p>
                <p class="fst-italic">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                  magna aliqua.
                </p>
                <ul>
                  <li><i class="bi bi-check-circle-fill"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                  <li><i class="bi bi-check-circle-fill"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                  <li><i class="bi bi-check-circle-fill"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
                </ul>
              </div>
              <div class="col-lg-4 order-1 order-lg-2 text-center">
                <img src="{{ asset('assets/Dashboard') }}/assets/img/features-5.svg" alt="" class="img-fluid">
              </div>
            </div>
          </div><!-- End Tab Content 5 -->

          <div class="tab-pane" id="tab-6">
            <div class="row gy-4">
              <div class="col-lg-8 order-2 order-lg-1">
                <h3>Reprehit</h3>
                <p>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                  culpa qui officia deserunt mollit anim id est laborum
                </p>
                <p class="fst-italic">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                  magna aliqua.
                </p>
                <ul>
                  <li><i class="bi bi-check-circle-fill"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                  <li><i class="bi bi-check-circle-fill"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                  <li><i class="bi bi-check-circle-fill"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
                </ul>
              </div>
              <div class="col-lg-4 order-1 order-lg-2 text-center">
                <img src="{{ asset('assets/Dashboard') }}/assets/img/features-6.svg" alt="" class="img-fluid">
              </div>
            </div>
          </div><!-- End Tab Content 6 -->

        </div>

      </div>
    </div>
    </section><!-- End Features Section -->

    <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>SID Infinity</h3>
              <p class="pb-3"><em>Memperluas jaringan internet di daerah anda</em></p>
              <p><br>
                <strong>Phone:</strong> 081296967484<br>
                <strong>Email:</strong> ndry@sid.net.id<br>
              </p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div>

        </div>
      </div>
    </div>


  </footer><!-- End Footer -->
  <script>
    window.onclick = function(event) {
  if (!event.target.matches('.profile-icon')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    for (var i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.style.display === 'block') {
        openDropdown.style.display = 'none';
      }
    }
  }
}
  </script>


  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/aos/aos.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('assets/Dashboard') }}/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/Dashboard') }}/assets/js/main.js"></script>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.cjs.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/aos/aos.esm.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('assets/LandingPage') }}/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/LandingPage') }}/assets/js/main.js"></script>


</body>

</html>
