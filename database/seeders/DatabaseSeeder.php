<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('role')->insert([
            [
                'role' => 'admin',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'role' => 'pelanggan',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ]);

        DB::table('paket')->insert([
            [
                'nama_paket' => 'Paket Sederhana',
                'harga' => '350000',
                'kecepatan' => '30mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Terbaik',
                'harga' => '420000',
                'kecepatan' => '50mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '500000',
                'kecepatan' => '100mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paket Complete',
                'harga' => '600000',
                'kecepatan' => '200mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '420000',
                'kecepatan' => '100mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '390000',
                'kecepatan' => '150mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '430000',
                'kecepatan' => '120mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '460000',
                'kecepatan' => '300mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '490000',
                'kecepatan' => '300mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '500000',
                'kecepatan' => '350mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Paling Untung',
                'harga' => '440000',
                'kecepatan' => '400mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '600000',
                'kecepatan' => '400mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '630000',
                'kecepatan' => '410mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '650000',
                'kecepatan' => '450mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '680000',
                'kecepatan' => '450mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '700000',
                'kecepatan' => '500mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '740000',
                'kecepatan' => '500mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '780000',
                'kecepatan' => '550mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Populer',
                'harga' => '800000',
                'kecepatan' => '600mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '800000',
                'kecepatan' => '600mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '840000',
                'kecepatan' => '650mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '860000',
                'kecepatan' => '700mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '880000',
                'kecepatan' => '700mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '900000',
                'kecepatan' => '750mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '930000',
                'kecepatan' => '750mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '950000',
                'kecepatan' => '800mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '980000',
                'kecepatan' => '850mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '970000',
                'kecepatan' => '850mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'nama_paket' => 'Pasti Complete',
                'harga' => '1000000',
                'kecepatan' => '850mbps',
                'kelebihan' => 'Murah dan Cepat',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ]);

    }
}
