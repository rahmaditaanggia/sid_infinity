<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->id('id_pembayaran');
            // $table->unsignedBigInteger('pelanggan_id');
            // $table->unsignedBigInteger('paket_id');
            // $table->integer('harga');
            // $table->enum('status',['Belum di bayar', 'Sudah di Bayar']);
            $table->unsignedBigInteger('pesanan_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pembayaran');
    }
};
