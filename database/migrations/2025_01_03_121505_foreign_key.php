<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id_role')->on('role')->onDelete('cascade')->onUpdate('cascade');
        });

        // Schema::table('pelanggan', function (Blueprint $table) {
        //     $table->foreign('paket_id')->references('id_paket')->on('paket')->onDelete('cascade')->onUpdate('cascade');
        // });

        Schema::table('pembayaran', function (Blueprint $table) {
            $table->foreign('pesanan_id')->references('id_pesanan')->on('pesanan')->onDelete('cascade')->onUpdate('cascade');
            // $table->foreign('pelanggan_id')->references('id_pelanggan')->on('pelanggan')->onDelete('cascade')->onUpdate('cascade');
            // $table->foreign('paket_id')->references('id_paket')->on('paket')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('pesanan', function (Blueprint $table) {
            $table->foreign('paket_id')->references('id_paket')->on('paket')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pelanggan_id')->references('id_pelanggan')->on('pelanggan')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('subscription', function (Blueprint $table) {
            $table->foreign('pesanan_id')->references('id_pesanan')->on('pesanan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
