<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared('
        CREATE TRIGGER Regis_To_Login_Pelanggan AFTER INSERT ON `pelanggan` FOR EACH ROW
            BEGIN
                INSERT INTO users
                set name = new.name,
                email = new.email,
                password = new.password,
                role_id = 2;
            END
        ');

        DB::unprepared('
        CREATE TRIGGER Regis_To_Login_Admin AFTER INSERT ON `admin` FOR EACH ROW
            BEGIN
                INSERT INTO users
                set name = new.name,
                email = new.email,
                password = new.password,
                role_id = 1;
            END
        ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
