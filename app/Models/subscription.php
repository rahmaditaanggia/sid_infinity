<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    protected $table = 'subscription';

    protected $primaryKey = 'id_subscription';

    protected $fillable = [
        'pembayaran_id',
        'subscription_start',
        'subscription_exp',
    ];

    public function daysUntilExpiration()
    {
        $expirationDate = Carbon::parse($this->expiration_date);
        $now = Carbon::now();

        return $now->diffInDays($expirationDate);
    }

}
