<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class expired extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Expired',
        );
    }

    public function content(): Content
    {
        return new Content(
            view: 'mail.exp',
            // with: [
            //     'orderName' => $this->order->name,
            //     'orderPrice' => $this->order->price,
            // ],
        );
    }

    public function attachments(): array
    {
        return [];
    }
}
