<?php

namespace App\Jobs;

use App\Mail\expired;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SubExp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $data;
    public $tries = 3;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle(): void
    {
        \Log::info($this->data);
        $email = new expired($this->data);
        Mail::to($this->data['email'])->send($email);
    }
}
