<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Pelanggan;
use App\Models\Pembayaran;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LandingController extends Controller
{
    public function landing()
    {
        $paket = Paket::all();
        return view('LandingPage', compact('paket'));
    }

    public function dashboard()
    {
        $paket = Paket::paginate(4);
        // dd($paket);
        return view('dashboard', compact('paket'));
    }
}
