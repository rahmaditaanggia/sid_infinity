<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\YourModel;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->input('search');
        $paket = Paket::where('nama_paket', 'like', '%' . $query . '%')->get();
        return view('detailpaket', ['paket' => $paket]);
    }
}