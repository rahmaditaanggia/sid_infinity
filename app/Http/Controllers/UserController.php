<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Models\Pesanan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function create()
    {
        return view('admin.user.user-create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'password' => 'required',
            'no_telp' => 'required'
        ]);

        Pelanggan::create($request->all());

        return redirect()->route('admin.member')->with('success','Product created successfully.');
    }

    public function edit($id_pelanggan)
    {
        $pelanggan = Pelanggan::where('id_pelanggan', $id_pelanggan)
        ->get();
        // dd($pelanggan);
        return view('admin.user.user-edit',compact('pelanggan'));
    }

    public function update(Request $request, Pelanggan $pelanggan)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required'
        ]);
        Pelanggan::where('id_pelanggan', $request->id_pelanggan)->update([
            'name' => $request->name,
            'email' => $request->email,
            'alamat' => $request->alamat
        ]);

        User::join('pelanggan', 'users.email', '=', 'pelanggan.email')->where('id_pelanggan', $request->id_pelanggan)->update([
            'users.name' => $request->name,
            'users.email' => $request->email,
        ]);

        return redirect()->route('admin.member')->with('success','Product updated successfully');
    }

    public function destroy($id_pelanggan)
    {
        $pelanggan = Pelanggan::find($id_pelanggan);

        // Pelanggan::join('users', 'pelanggan.email', '=', 'users.email')->where('id_pelanggan',$id_pelanggan)->delete();

        if (!$pelanggan) {
            // Handle jika pelanggan tidak ditemukan
            throw new \Exception('Pelanggan not found.');
        }

        // Hapus pelanggan berdasarkan email
        Pelanggan::where('email', $pelanggan->email)->delete();

        // Hapus user berdasarkan email
        User::where('email', $pelanggan->email)->delete();

        return redirect()->route('admin.member')->with('success','Product deleted successfully');
    }

    public function invoice()
    {
        $id = Auth::id();
        $id_pesanan = Auth::id();
        $pesanan = Pesanan::select('id_pesan', 'harga', 'status')->where("id_pesanan", $id_pesanan)->first();
        $info_pelanggan = User::select('name', 'email')->where("id", $id)->first();
        $nama_pelanggan = $info_pelanggan->name;
        $email_pelanggan = $info_pelanggan->email;
        $user = User::join('pelanggan', 'pelanggan.email', '=', 'users.email')->get();
        return view('invoice', [
            'nama_pelanggan' => $nama_pelanggan,
            'email_pelanggan' => $email_pelanggan,
        ]);
        // $id_pesanan = Auth::id();
        // $pesanan = Pesanan::find($id_pesanan);
        // return view('invoice', compact('pesanan'));
    }

    public function berhasil()
    {
        $tanggalPesan = Carbon::now()->format('l, d F Y');

        return view("berhasil", compact('tanggalPesan'));
    }

    public function gagal()
    {
        $tanggalPesan = Carbon::now()->format('l, d F Y');

        return view("gagal", compact('tanggalPesan'));
    }

}

