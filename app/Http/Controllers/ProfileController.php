<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function profile()
    {
        $id = Auth::id();
        $id_pelanggan = Auth::id();
        $info_pelanggan = User::select('name', 'email')->where("id", $id)->first();
        $pelanggan = Pelanggan::select('no_telp','alamat')->where("id_pelanggan", $id_pelanggan)->first();
        $nama_pelanggan = $info_pelanggan->name;
        $email_pelanggan = $info_pelanggan->email;
        $alamat_pelanggan = $pelanggan->alamat;
        $no_telp_pelanggan = $pelanggan->no_telp;
        $user = User::join('pelanggan', 'pelanggan.email', '=', 'users.email')->get();
        return view('profilepelanggan', [
            'id' => $id,
            'nama_pelanggan' => $nama_pelanggan,
            'email_pelanggan' => $email_pelanggan,
            'alamat_pelanggan' => $alamat_pelanggan,
            'no_telp_pelanggan' => $no_telp_pelanggan,
        ]);
    }

    public function edit($id)
	{
        $users = DB::table('pelanggan')->where('id_pelanggan', $id)->get();
        $roles = DB::table('role')->get();
        $subscription = DB::table('subscription')->get();

        return view('edit', compact('users', 'roles', 'subscription'));
	}

    public function update(Request $request)
    {

        DB::table('pelanggan')->where('id_pelanggan', $request->id_pelanggan)->update([
            'name' => $request->name,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'alamat' => $request->alamat,
        ]);

        DB::table('users')->where('id', $request->id_pelanggan)->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect('/profilepelanggan');
    }
}
