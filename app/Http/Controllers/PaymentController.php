<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Pembayaran;
use App\Models\Pesanan;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PaymentController extends Controller
{
    public function detailjual($id_paket)
    {
        $user = Auth::user();
        // dd($u);
        $pembayaran = Pembayaran::all();
        $paket = Paket::where('id_paket', $id_paket)
            ->get();
        $p = Paket::where('id_paket', $id_paket)
            ->first();


        $u = User::join('pelanggan', 'pelanggan.email', '=', 'users.email')
            ->select('pelanggan.no_telp', 'pelanggan.alamat', 'pelanggan.id_pelanggan', 'users.*')
            ->where('users.id', '=', $user->id)
            ->first();
        $pesan = Pesanan::where('paket_id', $p->id_paket)
            ->where('pelanggan_id', $u->id_pelanggan)
            // ->where('status', 'Belum di Bayar')
            ->first();

        if (!$pesan) {
            // Jika pesanan tidak ditemukan, buat pesanan baru

            $pesan = new Pesanan();
            $pesan->id_pesanan = date('Ymd') . mt_rand(1000, 9999);
            $pesan->paket_id = $p->id_paket;
            $pesan->pelanggan_id = $u->id_pelanggan;
            $pesan->harga = $p->harga;
            $pesan->status = 'Belum di bayar';
            $pesan->save();

            $harga = $p->harga ?? 0;
            if ($harga) {
                $total = intval($harga * 1.1);
            }
            $item = array(
                'id' => $p->id_paket,
                'name' => $p->nama_paket,
                'price' => $harga,
                'quantity' => 1,
                'subtotal' => $total,
            );

            $metadata = array(
                'item_id' => $pesan->id_pesanan,
                'user_id' => $pesan->id_pesanan
            );

            $params = array(
                'transaction_details' => array(
                    'order_id' => $pesan->id_pesanan,
                    'gross_amount' => $harga,
                    'id_paket' => $id_paket,
                ),
                'customer_details' => array(
                    'first_name' => $u->name,
                    'email' => $u->email,
                    'phone' => $u->no_telp,
                    'billing_address' => $u->alamat,
                ),
                'item_details' => array($item),
                'metadata' => $metadata,
            );
        } else {

            $pesan->id_pesan = 'P' . date('Ymd') . mt_rand(1000, 9999);
            $pesan->update();
            // $pesan->save();


            $harga = $p->harga ?? 0;

            if ($harga) {
                $total = intval($harga * 1.1);
            }
            $item = array(
                'id' => $p->id_paket,
                'name' => $p->nama_paket,
                'price' => $harga,
                'quantity' => 1,
                'subtotal' => $total,
            );

            $metadata = array(
                'item_id' => $pesan->id_pesanan,
                'user_id' => $pesan->id_pesanan
            );

            $params = array(
                'transaction_details' => array(
                    'order_id' => $pesan->id_pesan,
                    'gross_amount' => $harga,
                    // 'id_paket' => $id_paket,
                ),
                'customer_details' => array(
                    'first_name' => $u->name,
                    'email' => $u->email,
                    'phone' => $u->no_telp,
                    'billing_address' => $u->alamat,
                ),
                'item_details' => array($item),
                'metadata' => $metadata,
            );
        }

        \Midtrans\Config::$serverKey = config('midtrans.server_key');
        \Midtrans\Config::$isProduction = false;
        \Midtrans\Config::$isSanitized = true;
        \Midtrans\Config::$is3ds = true;

        $snapToken = \Midtrans\Snap::getSnapToken($params);
        return view('detailpenjualan', compact('paket', 'snapToken'));
    }

    public function callback(Request $request)
    {
        $serverKey = config('midtrans.server_key');
        $hashed = hash("sha512", $request->order_id . $request->status_code . $request->gross_amount . $serverKey);

        if ($hashed === $request->signature_key) {
            if ($request->transaction_status === 'capture') {

                $metadata = $request->input('metadata');
                $item = isset($metadata['item_id']) ? $metadata['item_id'] : nulll;
                $user = isset($metadata['user_id']) ? $metadata['user_id'] : nul;
                $p = Pesanan::find($item);

                if ($p) {
                    if ($p->status == 'Belum di bayar') {
                        // Process for 'Belum di Bayar'
                        $p->update(['status' => 'Sudah di Bayar']);

                        $subrek = new Subscription();
                        $subrek->pesanan_id = $user;
                        $subrek->subscription_start = now();
                        $subrek->subscription_exp = now()->addDays(30);
                        $subrek->save();
                    } elseif ($p->status == 'Sudah di Bayar') {
                        // Process for 'Sudah di Bayar'
                        $subscription = Subscription::where('pesanan_id', '=', $user)->first();
                        $subscription->subscription_exp = Carbon::parse($subscription->subscription_exp);
                        $subscription->update(['subscription_exp' => $subscription->subscription_exp->addDays(30)]);
                    }
                }
            }
        }
    }
}
