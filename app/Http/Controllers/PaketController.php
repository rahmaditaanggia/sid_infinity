<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use Illuminate\Http\Request;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $paket = Paket::all();
        return view('admin.paket.paket-create', compact('paket'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_paket' => 'required',
            'harga' => 'required',
            'kelebihan' => 'required',
        ]);

        Paket::create($request->all());

        return redirect()->route('admin.paket')->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     */
    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id_paket)
    {
        $paket = Paket::where('id_paket', $id_paket)
        ->get();
        // dd($paket);
        return view('admin.paket.paket-edit',compact('paket'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $request->validate([
            'nama_paket' => 'required',
            'harga' => 'required',
            'kecepatan' => 'required',
            'kelebihan' => 'required',
        ]);
        Paket::where('id_paket', $request->id_paket)->update([
            'nama_paket' => $request->nama_paket,
            'harga' => $request->harga,
            'kecepatan' => $request->kecepatan,
            'kelebihan' => $request->kelebihan
        ]);

        return redirect()->route('admin.paket')->with('success','Product updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id_paket)
    {
        $paket = Paket::find($id_paket);

        // Pelanggan::join('users', 'paket.email', '=', 'users.email')->where('id_pelanggan',$id_pelanggan)->delete();

        if (!$paket) {
            // Handle jika paket tidak ditemukan
            throw new \Exception('Pelanggan not found.');
        }

        // Hapus paket berdasarkan email
        Paket::where('id_paket', $paket->id_paket)->delete();

        return redirect()->route('admin.paket')->with('success','Product deleted successfully');
    }

    public function detailpaket(Request $request)
    {
        $paket = Paket::all();
        $query = $request->input('query');
        $results = Paket::where('nama_paket', 'like', '%' . $query . '%')->get();
        return view('detailpaket', ['results' => $results], compact('paket'));
    }
}
