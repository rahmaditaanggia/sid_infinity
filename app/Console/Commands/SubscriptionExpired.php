<?php

namespace App\Console\Commands;

use App\Jobs\SubExp;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SubscriptionExpired extends Command
{
    protected $signature = 'app:subscription-expired';

    protected $description = 'Command description';

    public function handle()
    {
        $subrek = Subscription::all();

        foreach ($subrek as $s) {
                dispatch(new SubExp($s));
        }

        $this->info('Expired Email Sudah Terkirim');
    }
}
