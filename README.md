# SID_INFINITY
# Website pembelian jaringan internet
 aplikasi yang memungkinkan pengguna untuk membeli akses internet wifi melalui ponsel atau perangkat lainnya. 

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Fitur Aplikasi :
- Pemilihan paket
- berbagai metode pembayaran
- pemantauan penggunaan 
- riwayat transaksi
- Promosi dan penawaran

## Tech
Aplikasi ini dibangun dengan menggunakan :
- [XAMPP] -  paket perangkat lunak yang digunakan untuk membuat dan mengelola lingkungan pengembangan web lokal
- [Google Chrome] - salah satu peramban web (web browser) yang paling populer dan banyak digunakan di seluruh dunia.
- [HTML] -  (HyperText Markup Language) adalah bahasa markup yang digunakan untuk membuat dan mendesain halaman web.
- [CSS] - (Cascading Style Sheets) adalah bahasa yang digunakan untuk mengatur tampilan dan format halaman web yang dibuat dengan HTML.
- [PHP] - (Hypertext Preprocessor) adalah bahasa pemrograman server-side yang banyak digunakan untuk pengembangan web.
- [BOOTSTRAP] - kerangka kerja (framework) front-end yang digunakan untuk membangun antarmuka pengguna (UI) yang responsif dan menarik secara visual.
- [NGROK] - perangkat lunak yang memungkinkan Anda membuat saluran terowongan yang aman dari internet ke mesin Anda lokal.
- [MAILTRAP] - layanan yang menyediakan server SMTP untuk menangkap email selama pengembangan dan pengujian aplikasi.

## Requirement
- XAMPP 2.4.54 or later
- PHP 8.1.12 or later
- Bootstrap 5 or later

## Credit
- Anggia Rahmadita
- Narayana agung taja santoso


























 
 
